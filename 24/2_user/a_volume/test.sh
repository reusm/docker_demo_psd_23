docker run \
	-it \
	--gpus all \
	--volume /home/reusm/code/VMamba:/workspace/vmamba \
	vmamba:build_b \
	pytest vmamba/selective_scan
