# Alternative to get those values.
UID=$(id -u)
GID=$(id -g)
USERNAME=$(id -un)

docker build . \
	--tag vmamba:user \
	--build-arg UID=${UID} \
	--build-arg GID=${GID} \
	--build-arg USERNAME=${USERNAME}
