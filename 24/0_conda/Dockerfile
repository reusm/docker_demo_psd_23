FROM continuumio/miniconda3:23.10.0-1

RUN conda create --name vmamba

# The 2 lines below are only required because we set the conda env in a Docker image
SHELL [ "conda", "run", "-n", "vmamba", "/bin/bash", "-c" ]
ENV CONDA_ALWAYS_YES="true"

RUN conda install pytorch==1.13.1 torchvision==0.14.1 torchaudio==0.13.1 pytorch-cuda=11.7 -c pytorch -c nvidia
RUN conda install gxx==11.4.0 -c conda-forge

# nvidia conda's channel does not handle dependencies for a specific version of cuda (1.X), so we need to specify the package version by hand.
RUN conda install cuda-cudart-dev=11.7 cuda-cccl=11.7 cuda-cudart=11.7 cuda-compiler=11.7 cuda-cuobjdump=11.7 cuda-cuxxfilt=11.7 cuda-nvcc=11.7 cuda-nvprune=11.7 cuda-demo-suite=11.7 cuda-documentation=11.7 cuda-driver-dev=11.7 cuda-gdb=11.7 cuda-memcheck=11.7 cuda-nsight=11.7 cuda-nsight-compute=11.7 cuda-nvdisasm=11.7 cuda-nvml-dev=11.7 cuda-nvprof=11.7 cuda-nvvp=11.7 cuda-sanitizer-api=11.7 -c nvidia && \
    # some packages does not have a semantic version that follow cuda's semantic version. Thus, the packages' versions are not manually handled.
    conda install cuda=11.7 -c nvidia && \
		conda clean --all

# RUN conda install cuda-cudart-dev=11.7 cuda-cccl=11.7 cuda-cudart=11.7 -c nvidia
# RUN conda install cuda-compiler=11.7 cuda-cuobjdump=11.7 cuda-cuxxfilt=11.7 cuda-nvcc=11.7 cuda-nvprune=11.7 -c nvidia
# RUN conda install cuda-demo-suite=11.7 cuda-documentation=11.7 cuda-driver-dev=11.7 cuda-gdb=11.7 -c nvidia
# RUN conda install cuda-memcheck=11.7 cuda-nsight=11.7 cuda-nsight-compute=11.7 cuda-nvdisasm=11.7 cuda-nvml-dev=11.7 cuda-nvprof=11.7 -c nvidia
# RUN conda install cuda-nvvp=11.7 cuda-sanitizer-api=11.7
# # some packages does not have a semantic version that follow cuda's semantic version. Thus, the packages' versions are not manually handled.
# RUN conda install cuda=11.7 -c nvidia

COPY requirements.txt .
ENV CUDA_HOME=/opt/conda/envs/vmamba
RUN git clone https://github.com/MzeroMiko/VMamba vmamba && \
		pip install --no-cache-dir -r requirements.txt && \
		pip install --no-cache-dir vmamba/selective_scan && \
		rm -rf vmamba
