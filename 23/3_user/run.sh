docker run  \
  --user "$(id -u):$(id -g)" \
  -it \
  --gpus all \
  --volume /home/reusm/code/demo/docker/data:/data \
  --volume /home/reusm/code/demo/docker/code:/workspace \
  --volume vscode_server:/home/reusm/.vscode-server \
  --volume python_cache:/home/reusm/.cache \
  yolonas:user


