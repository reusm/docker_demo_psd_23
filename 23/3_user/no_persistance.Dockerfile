FROM pytorch/pytorch:1.13.1-cuda11.6-cudnn8-runtime

RUN apt update && apt install gcc -y && \
  apt-get clean && rm -rf /var/lib/apt/lists/* 

RUN pip install --no-cache super-gradients
RUN pip uninstall opencv-python -y && pip install --no-cache opencv-python-headless

ARG UID
ARG GID
ARG USERNAME
RUN addgroup \
    --quiet \
    --gid ${GID} \
    ${USERNAME} && \
    adduser \
    --quiet \
    --uid ${UID} \
    --gid ${GID} \
    ${USERNAME}
# RUN mkdir -p /home/${USERNAME}/.cache/clip /home/${USERNAME}/.vscode-server && \
#     chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}/.cache && \
#     chown -R ${USERNAME}:${USERNAME} /home/${USERNAME}/.vscode-server
