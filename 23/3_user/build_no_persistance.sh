UID=$(id -u)
GID=$(id -g)
USERNAME=$(id -un)

docker build . \
  --file ./no_persistance.Dockerfile \
  --tag yolonas:no_persistance \
  --build-arg UID=$UID \
  --build-arg GID=$GID \
  --build-arg USERNAME=$USERNAME

