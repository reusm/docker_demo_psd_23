UID=$(id -u)
GID=$(id -g)
USERNAME=$(id -un)

docker build . \
  --tag yolonas:user \
  --build-arg UID=$UID \
  --build-arg GID=$GID \
  --build-arg USERNAME=$USERNAME

